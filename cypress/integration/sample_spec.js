describe('My First Test', () => {
  it('Visits my local host', () => {
    cy.visit('/');

    cy.get('input[name="email"]')
      .type('zero@cool.gg')
      .should('have.value', 'zero@cool.gg');

    cy.get('input[name="password"]')
      .type('MyPass99')
      .should('have.value', 'MyPass99');

    cy.server();
    cy.route('POST', '**/verifyPassword**').as('verifyPassword');
    cy.route('POST', '**/getAccountInfo**').as('getAccountInfo');

    cy.get('input[name="login"]')
      .click();

    cy.wait('@verifyPassword');
    cy.wait('@getAccountInfo');

    cy.get('.user')
      .should('contain', 'zero@cool.gg');
  })
});
