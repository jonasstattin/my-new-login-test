import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Message from './Message';
import { validateLogin } from '../utils/validation';
import api from '../api/firebase';

class Login extends Component {
  static propTypes = {
    loginSuccessful: PropTypes.func.isRequired
  };

  state = {
    email: '',
    password: '',
    message: {
      type: '',
      body: ''
    }
  };

  componentDidMount() {
    api.auth.onAuthStateChanged(authUser => {
      if (authUser) {
        this.setState(
          {
            message: { type: 'SUCCESS', body: 'Logged in' },
            email: '',
            password: ''
          },
          () => {
            this.props.loginSuccessful(authUser.email);
          }
        );
      }
    });
  }

  onSignIn = e => {
    e.preventDefault();
    const { email, password } = this.state;
    if (validateLogin(email, password)) {
      api.signIn(email, password)
        .catch(({message}) => {
          this.setState({
            message: { type: 'ERROR', body: message },
            email: '',
            password: ''
          });
        });
    } else {
      this.setState({
        message: { type: 'ERROR', body: 'Wrong password or email' },
        email: '',
        password: ''
      });
    }
  };

  onSignUp = e => {
    e.preventDefault();
    const { email, password } = this.state;
    if (validateLogin(email, password)) {
      api.signUp(email, password)
        .catch(({message}) => {
          this.setState({
            message: { type: 'ERROR', body: message },
            email: '',
            password: ''
          });
        });
    } else {
      this.setState({
        message: { type: 'ERROR', body: 'Wrong password or email' },
        email: '',
        password: ''
      });
    }
  };

  handleChange = ({ target }) => this.setState({ [target.name]: target.value });

  render() {
    return (
      <form
        data-test="form"
        className="flex flex-col items-center w-1/2 mx-auto mt-8"
      >
        <Message message={this.state.message} />
        <label htmlFor="email" className="mb-4 w-full">
          Email <br />
          <input
            type="email"
            name="email"
            id="email"
            onChange={this.handleChange}
            value={this.state.email}
            placeholder="zero@cool.gg"
            className="my-4 p-2 rounded shadow border w-full"
          />
        </label>
        <label htmlFor="password" className="mb-4  w-full">
          Password <br />
          <input
            type="password"
            name="password"
            id="password"
            value={this.state.password}
            onChange={this.handleChange}
            placeholder="8 length, 1 uppercase, 1 digit"
            className="mb-8 mt-4 p-2 rounded shadow border  w-full"
          />
        </label>
        <div>
          <input
            onClick={this.onSignIn}
            type="button"
            name="login"
            value="Login"
            className="bg-purple hover:bg-purple-dark text-white font-bold py-2 px-4 rounded mx-2"
          />
          <input
            onClick={this.onSignUp}
            type="button"
            name="signup"
            value="Sign Up"
            className="bg-pink hover:bg-pink-dark text-white font-bold py-2 px-4 rounded mx-2"
          />
        </div>
      </form>
    );
  }
}

export default Login;
