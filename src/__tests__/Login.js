import React from 'react';
import { mount } from 'enzyme';
import Login from '../components/Login';

it('simulate login failed', () => {
  const fakeLogin = jest.fn();
  const wrapper = mount(<Login loginSuccessful={fakeLogin} />);
  const emailEvent = {target: {name: "email", value: "zero@cool.gg"}};
  const passwordEvent = {target: {name: "password", value: "jaahaja!"}};
  wrapper.find('input[name="email"]').simulate('change', emailEvent);
  wrapper.find('input[name="password"]').simulate('change', passwordEvent);  
  wrapper.find('input[name="login"]').simulate('click');
  expect(wrapper.find('.error').exists()).toBeTruthy();
});

it('simulate login success', () => {
  const fakeLogin = jest.fn();
  const wrapper = mount(<Login loginSuccessful={fakeLogin} />);
  const emailEvent = {target: {name: "email", value: "zero@cool.gg"}};
  const passwordEvent = {target: {name: "password", value: "MyPass99"}};

  wrapper.instance().onSignIn = jest.fn(() => {
    wrapper.setState({
      message: { type: 'SUCCESS', body: 'Logged in' },
      email: '',
      password: ''
    });
  });
  wrapper.instance().forceUpdate();
  wrapper.update();

  wrapper.find('input[name="email"]').simulate('change', emailEvent);
  wrapper.find('input[name="password"]').simulate('change', passwordEvent);  
  wrapper.find('input[name="login"]').simulate('click');
  expect(wrapper.find('.success').exists()).toBeTruthy();
});