import * as firebase from 'firebase/app';
import 'firebase/auth';

const config = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: 'my-new-login-test.firebaseapp.com',
  databaseURL: 'https://my-new-login-test.firebaseio.com',
  projectId: 'my-new-login-test',
  storageBucket: 'my-new-login-test.appspot.com',
  messagingSenderId: '868183621137'
};

class Firebase {
  constructor() {
    firebase.initializeApp(config);

    this.auth = firebase.auth();
  }
  
  signUp = (email, password) => this.auth.createUserWithEmailAndPassword(email, password);

  signIn = (email, password) => this.auth.signInWithEmailAndPassword(email, password);

  signOut = () => this.auth.signOut();
};

const api = new Firebase();

export default api;
